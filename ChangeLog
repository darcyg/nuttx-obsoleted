ChangeLog
=========

2014-09-01:  Removed support for 8051 from main source tree.

  REASON: There are three:
  1. The older 8051 architecture has a hardware stack and required special
     case handling throughout the OS.  With a current focus on supporting
     true processes, the limitations of the 8051 architecture are becoming
     obstacles to the advancement of the design.
  2. The port was at one time functional, but never worked reliably.  It
     would occasionally exceed the limited 8051 stack size.  It is possible,
     however, that this problem could be fixed with additional tuning.
  3. The 8051 is basically a bad architecture for multi-tasking.  Since
     the 8051 has a hardware stack, the entire stack has to be copied on
     contex switches.
  4. I do not thing that anyone has ever used the port and because of bit
     rot, it is not even certain that it is still function.

  NEW HOME:
    Obsoleted/nuttx/configs/8051 and Obsoleted/nuttx/configs/pjrc-8051

  REMOVAL PATCH:
    Obsoleted/Patches/Remove-8051-2014-9-1.patch

2014-09-05:  Removed support for the 16z board from the main source tree.

  REASON:
    The port is not yet ready for use.  It may return to the NuttX source
    tree at some point in the future.

  NEW HOME:
    Obsoleted/nuttx/configs/16z

  REMOVAL PATCH:
    Obsoleted/Patches/Remove-16z-2014-9-5.patch

2014-11-10:  Removed support for the stm32f100rc_generic board configuration

  REASON
    This support was obsoleted because of a decision to stop support of
    generic board configurations. Generic board configurations do not
    provide support for any specific hardware but can be useful only if
    there are not other examples for the setup for a particular
    architecture.

  NEW HOME:
   Obsoleted/nuttx/configs/stm32f100rc_generic

  REMOVAL PATCH
    Obsoleted/Patches/Remove-stm32f100rc_generic-2014-11-10.patch

2015-01-14: Removed support for the px4fmu-v2_upstream board configuration

  REASON
    This is not the official configuration for the PX4 board and has led
    to confusion by NuttX users.  The board configuration also requires
    some ongoing maintenance and customization to support ongoing PX4
    testing and evaluation.  It is best retained the PX4 repositories
    where it can be properly maintained and not in the upstream NuttX
    repository.

  NEW HOME
    Obsoleted/nuttx/configs/px4fmu-v2_upstream

  REMOVAL PATCH
    Obsoleted/Patches/Remove-stm32f100rc_generic-2014-11-10.patch

2015-04-13: Remove fragmentary support for the Intel Galileo bord.

  REASON
    That port is not going to happen (I don't even have the Galileo
    board anymore).

  NEW HOME
    Obsoleted/nuttx/configs/galileo

  REMOVAL PATCH
     Obsoleted/Patches/Remove-Galileo-2015-04-13.patch

2016-01-27: Remove apps/system/ramtron

  REASON
    First, this is an inappropriate application because it uses
    purely internal, operating system interfaces.  It was really
    a part of the OS board support in the wrong location.

    But then the SPI internal interfaces changed so that it is
    now (correctly) impossible to support such an application.
    Basically, the up_spiinitialize() internal interface is
    gone.

  NEW HOME
    None..  See the Removal Patch.

  REMOVAL PATCH
    Obsoleted/Patches/Remove-appsramtron-2016-01-27.patch

2016-03-29: Remove nuttx/configs/vsn

  REASON
    This configuration is not being maintained and does things in
    ways that are not now considered standard.  I doubt that the
    VSN boards are even still available.  Thus, the configuration
    is both useless and a maintenance problem for me.

  NEW HOME
    Obsoleted/nuttx/configs/vsn

  REMOVAL PATH
    Obsoleted/Patches/Remove-vsn-config-2016-03-29.patch

2016-03-29: Remove apps/system/sdcard

  REASON
    This applicatin violates the POSIX OS/Application interface by
    calling directly into the OS internal functions.

  NEW HOME
    Obsoleted/apps/system/sdcard

  REMOVAL PATCH
    Obsoleted/Patches/Remove-apps-sdcard-2016-03-29.patch

2016-04-12: Remove nuttx/configs/nucleus2g

  REASON
    Removed the Nucleus2G configuration because there has not been any
    activity with the commercial board in a few years and it no longer
    appears to be available from the 2g-eng.com website.  Since the board
    is commercial and no longer publically available, it no longer qualifies
    for inclusion in the open source repositories.

  NEW HOME
    Obsoleted/nuttx/nuttx/configs/nucleus2g

  REMOVAL PATCH
    Obsoleted/Patches/Remove-configs-nucleus2g-2016-04-12.patch.

2016-06-03: Remove apps/system/flash_eraseall

  REASON
    This tool is useful, but violates the OS/application interface.  This
    can be replaced with an MDIOC_BULKERASE IOCTL call on any MTD-based
    driver instance.

    NOTE: apps/system/flash_eraseall was subsequently replaced in a slightly
    different form and so could probably be removed from the Obsoleted
    repository.

  NEW HOME
    Obsoleted/apps/system/flash_eraseall

  REMOVAL PATCH
    Obsoleted/Patches/Remove-appsflasherasall-2016-06-02.patch

2016-06-03:  Remove nuttx/drivers/mtd/flash_eraseall.c

  REASON
    This has never been used within the OS and is simply a wrapper around
    the MDIOC_BULKERASE IOCTL command.  It used to be called (only) from
    apps/system/flash_eraseall, but that has been also removed because it
    violated the OS/applicatin interface -- by calling flash_eraseall().

  NEW HOME
    Obsoleted/nuttx/drivers/mtd/flash_eraseall.c

  REMOVAL PATCH
    Obsoleted/Patches/Remove-mtdflasheraseall-2016-06-03.patch

2016-12-02:  Remove RGMP

  REASON
    I don't believe anyone has ever used the RGMP code in the NuttX
    repository.  The RGMP project has the last working NuttX code and
    that project has not been updated in several years.  It appears to
    be abandoned.  Keep this untested, abandoned code in the NuttX
    repository is a maintenance problem.

  NEW HOME:
    Obsoleted/nuttx/arch/rgmp
    Obsoleted/nuttx/drivers/net/e1000.c, e1000.h, vnet.c
    Obsoleted/nuttx/configs/rgmp
    Obsoleted/arch/examples/rgmp

  REMOVAL PATCHES:
    Obsoleted/Patches/Remove-RGMP-2016-12-02.patch
    Obsoleted/Patches/Remove-RGMP-Example-2016-12-02.patch

2016-12-13:  Remove Calypso architecture and support for all Calypso boards

  REASON
    This was a good idea that never caught on.  I had hoped that NuttX
    would create some interest and the port would mature.  But never
    happened.  It was a very incomplete, very low effort port that never
    developed.  Now those only Calypso platforms are obsolete so there
    is no point keeping this around

  NEW HOME:
    Obsoleted/nuttx/arch/arm/include/calypso
    Obsoleted/nuttx/arch/arm/src/calypso
    Obsoleted/nuttx/configs/compal_e86
    Obsoleted/nuttx/configs/compal_e88
    Obsoleted/nuttx/configs/compal_e99
    Obsoleted/nuttx/configs/pirelli-dpl10
    Obsoleted/nuttx/drivers/sercomm
    Obsoleted/nuttx/include/nuttx/sercomm

  REMOVAL PATCHES:
    Obsoleted/Patches/Remove-Calypso-arch-2016-12-13.patch
    Obsoleted/Patches/Remove-Calypso-configs-2016-12-13.patch
